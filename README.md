# ShrDoneKeyboard

[![CI Status](https://img.shields.io/travis/Ushashravan/ShrDoneKeyboard.svg?style=flat)](https://travis-ci.org/Ushashravan/ShrDoneKeyboard)
[![Version](https://img.shields.io/cocoapods/v/ShrDoneKeyboard.svg?style=flat)](https://cocoapods.org/pods/ShrDoneKeyboard)
[![License](https://img.shields.io/cocoapods/l/ShrDoneKeyboard.svg?style=flat)](https://cocoapods.org/pods/ShrDoneKeyboard)
[![Platform](https://img.shields.io/cocoapods/p/ShrDoneKeyboard.svg?style=flat)](https://cocoapods.org/pods/ShrDoneKeyboard)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ShrDoneKeyboard is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ShrDoneKeyboard'
```

## Author

Ushashravan, shravanios777@gmail.com

## License

ShrDoneKeyboard is available under the MIT license. See the LICENSE file for more info.
